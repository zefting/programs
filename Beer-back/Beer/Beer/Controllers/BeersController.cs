﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beer.Data;
using Beer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;



namespace Beer.Controllers
{   
    [Route("api/beers")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerRepo _repository;

        public BeersController(IBeerRepo repository)
        {
            _repository = repository;
        }

        // GET api/beers
        [HttpGet]
        public ActionResult <IEnumerable<Beers>> GetAllBeers()
        {
            var beerItems = _repository.GetAllBeers();
            return Ok(beerItems);
        }

        // GET api/beers/5
        [HttpGet("{id}")]
        public ActionResult <Beers> GetBeerById (int Beer_id)
        {
            var beerItem = _repository.GetBeerById(Beer_id);
            return Ok(beerItem);
        }

    }
}
