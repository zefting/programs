﻿using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Data
{
    public interface IBeerRepo
    {
        IEnumerable<Beers> GetAllBeers();
        Beers GetBeerById(int Beer_id);
    }
}
