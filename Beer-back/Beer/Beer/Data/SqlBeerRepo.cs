﻿using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Data
{
    public class SqlBeerRepo : IBeerRepo
    {
        private readonly BeerContext _context;

        public SqlBeerRepo(BeerContext context)
        {
            _context = context;
        }
        public IEnumerable<Beers> GetAllBeers()
        {
            return _context.Beers.ToList();
        }

        public Beers GetBeerById(int Beer_id)
        {
            return _context.Beers.FirstOrDefault(P => P.BeerId == Beer_id);
        }
    }
}
