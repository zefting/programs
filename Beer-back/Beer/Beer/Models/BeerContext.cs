﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Beer.Models
{
    public partial class BeerContext : DbContext
    {
        public BeerContext()
        {
        }

        public BeerContext(DbContextOptions<BeerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BeerOrders> BeerOrders { get; set; }
        public virtual DbSet<BeerType> BeerType { get; set; }
        public virtual DbSet<Beers> Beers { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BeerOrders>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("beer_orders");

                entity.HasIndex(e => e.BeerId)
                    .HasName("Beer_Order_Beer");

                entity.HasIndex(e => e.OrderId)
                    .HasName("Beer_Order_Order");

                entity.Property(e => e.BeerId)
                    .HasColumnName("Beer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrderId)
                    .HasColumnName("Order_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Beer)
                    .WithMany()
                    .HasForeignKey(d => d.BeerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Beer_Order_Beer");

                entity.HasOne(d => d.Order)
                    .WithMany()
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Beer_Order_Order");
            });

            modelBuilder.Entity<BeerType>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("PRIMARY");

                entity.ToTable("beer_type");

                entity.Property(e => e.TypeId)
                    .HasColumnName("Type_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Beskrivelse)
                    .HasMaxLength(225)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(225)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Beers>(entity =>
            {
                entity.HasKey(e => e.BeerId)
                    .HasName("PRIMARY");

                entity.ToTable("beers");

                entity.HasIndex(e => e.TypeId)
                    .HasName("Type");

                entity.Property(e => e.BeerId)
                    .HasColumnName("Beer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnType("int(11)");

                entity.Property(e => e.TypeId)
                    .HasColumnName("Type_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Beers)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Type");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PRIMARY");

                entity.ToTable("employees");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("Employee_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("First Name")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("Last Name")
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Rang)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PRIMARY");

                entity.ToTable("orders");

                entity.HasIndex(e => e.EmployeeId)
                    .HasName("Employee");

                entity.Property(e => e.OrderId)
                    .HasColumnName("Order_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("Created_at")
                    .HasDefaultValueSql("'current_timestamp()'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("Employee_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Paid)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("Updated_at")
                    .HasDefaultValueSql("'NULL'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Employee");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
