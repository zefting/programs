﻿using System;
using System.Collections.Generic;

namespace Beer.Models
{
    public partial class BeerOrders
    {
        public int BeerId { get; set; }
        public int OrderId { get; set; }

        public virtual Beers Beer { get; set; }
        public virtual Orders Order { get; set; }
    }
}
