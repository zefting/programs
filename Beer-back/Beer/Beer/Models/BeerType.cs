﻿using System;
using System.Collections.Generic;

namespace Beer.Models
{
    public partial class BeerType
    {
        public BeerType()
        {
            Beers = new HashSet<Beers>();
        }

        public int TypeId { get; set; }
        public string Type { get; set; }
        public string Beskrivelse { get; set; }

        public virtual ICollection<Beers> Beers { get; set; }
    }
}
