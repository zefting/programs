﻿using System;
using System.Collections.Generic;

namespace Beer.Models
{
    public partial class Beers
    {
        public int BeerId { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public int Price { get; set; }

        public string Beskrivelse { get; set; }

        public virtual BeerType Type { get; set; }
    }
}
