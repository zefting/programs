﻿using System;
using System.Collections.Generic;

namespace Beer.Models
{
    public partial class Employees
    {
        public Employees()
        {
            Orders = new HashSet<Orders>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Rang { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
