﻿using System;
using System.Collections.Generic;

namespace Beer.Models
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public byte? Paid { get; set; }
        public int EmployeeId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public virtual Employees Employee { get; set; }
    }
}
