﻿CREATE TABLE [dbo].[Table4]
(
	[Employee_id] INT NOT NULL PRIMARY KEY, 
    [First name] NVARCHAR(50) NOT NULL, 
    [Position] INT NOT NULL, 
    [Last Name] NVARCHAR(50) NOT NULL
)
